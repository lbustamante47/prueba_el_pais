<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Actividad;
use App\Entity\Categoria;
use App\Repository\ActividadRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;



class ActividadController extends AbstractController
{
    /**
     * @Route("/actividad", name="actividad")
     */
    public function registrar_actividad(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $actividad = new Actividad();

        $form = $this->createFormBuilder($actividad)
            ->add('categoria', EntityType::class, ['class' => Categoria::class, 'choice_label' => 'nombre', 'label' => 'Categoria'])
            ->add('nombre', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('fecha_tarea', DateType::class, ['label' => 'Fecha'])
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();
    
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $actividad = $form->getData();
            $entityManager->persist($actividad);
            $entityManager->flush();
        
            return $this->redirectToRoute('ver_actividades');
        }
        
        return $this->render('actividad/actividad.html.twig', ['form' => $form->createView(),]);
    }

    /**
     * @Route("/ver_actividades", name="ver_actividades")
     */
    public function ver_actividades()
    {
        $actividades = $this->getDoctrine()->getRepository(Actividad::class)->findAll();
        return $this->render('actividad/ver_actividades.html.twig', ['actividades' => $actividades]);

    }

    /**
     * @Route("/actualizar_actividad/{id}", name="actualizar_actividad")
     */
    public function actualizar_actividad(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $actividad = $entityManager->getRepository(Actividad::class)->find($id);
        
        if (!$actividad) {
            throw $this->createNotFoundException('No Existen la actividad con id'.$id);
        }
        $form = $this->createFormBuilder($actividad)
            ->add('categoria', EntityType::class, ['class' => Categoria::class, 'choice_label' => 'nombre', 'label' => 'Categoria'])
            ->add('nombre', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('fecha_tarea', DateType::class, ['label' => 'Fecha'])
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $actividad = $form->getData();
            $entityManager->flush();
            
            return $this->redirectToRoute('ver_actividades');
        }
    
        return $this->render('actividad/actividad.html.twig', ['form' => $form->createView(),]);   
    }

    /**
     * @Route("/eliminar_actividad/{id}", name="eliminar_actividad")
     */
    public function eliminar_actividad($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $actividad = $entityManager->getRepository(Actividad::class)->find($id);

        $entityManager->remove($actividad);
        $entityManager->flush();

        return $this->redirectToRoute('ver_actividades');
    }

}