<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Categoria;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoriaController extends AbstractController
{
    /**
     * @Route("/categoria", name="categoria")
     */
    public function registrar(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categoria = new Categoria();

        $form = $this->createFormBuilder($categoria)
            ->add('nombre', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();
    
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoria = $form->getData();
            $entityManager->persist($categoria);
            $entityManager->flush();
        
            return $this->redirectToRoute('ver_categorias');
        }
        
        return $this->render('categoria/categoria.html.twig', ['form' => $form->createView(),]);
    }

    /**
     * @Route("/ver_categorias", name="ver_categorias")
     */
    public function ver_categorias()
    {
        $categorias = $this->getDoctrine()->getRepository(Categoria::class)->findAll();
        return $this->render('categoria/ver_categorias.html.twig', ['categorias' => $categorias]);
    
    }

    /**
     * @Route("/actualizar_categoria/{id}", name="actualizar_categoria")
     */
    public function actualizar_categoria(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categoria = $entityManager->getRepository(Categoria::class)->find($id);
        
        if (!$categoria) {
            throw $this->createNotFoundException('No Existen la categoria con id'.$id);
        }
        $form = $this->createFormBuilder($categoria)
            ->add('nombre', TextType::class)
            ->add('descripcion', TextareaType::class)
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoria = $form->getData();
            $entityManager->flush();
            
            return $this->redirectToRoute('ver_categorias');
        }
    
        return $this->render('categoria/categoria.html.twig', ['form' => $form->createView(),]);   
    }

    /**
     * @Route("/eliminar_categoria/{id}", name="eliminar_categoria")
     */
    public function eliminar_categoria($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $categoria = $entityManager->getRepository(Categoria::class)->find($id);

        $entityManager->remove($categoria);
        $entityManager->flush();

        return $this->redirectToRoute('ver_categorias');
    }
}
