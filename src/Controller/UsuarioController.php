<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Usuario;
use App\Entity\Persona;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UsuarioController extends AbstractController
{

	/**
     * @Route("/usuario", name="usuario")
     */
    public function registrar_usuario(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $usuario = new Usuario();

        $form = $this->createFormBuilder($usuario)
            ->add('usuario', TextType::class)
            ->add('contrasenia', PasswordType::class, ['label' => 'Contraseña'])
            ->add('persona', EntityType::class, ['class' => Persona::class, 'choice_label' => 'nombres', 'label' => 'Nombre persona'])
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();
    
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $entityManager->persist($usuario);
            $entityManager->flush();
        
            return $this->redirectToRoute('ver_usuarios');
        }
        
        return $this->render('usuario/usuario.html.twig', ['form' => $form->createView(),]);
    }

    /**
     * @Route("/ver_usuarios", name="ver_usuarios")
     */
    public function ver_usuarios()
    {
        $usuarios = $this->getDoctrine()->getRepository(Usuario::class)->findAll();
        return $this->render('usuario/ver_usuarios.html.twig', ['usuarios' => $usuarios]);

    }

    /**
     * @Route("/actualizar_usuario/{id}", name="actualizar_usuario")
     */
    public function actualizar_usuario(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $usuario = $entityManager->getRepository(Usuario::class)->find($id);
        
        if (!$usuario) {
            throw $this->createNotFoundException('No Existen el usuario con id'.$id);
        }
        $form = $this->createFormBuilder($usuario)
            ->add('usuario', TextType::class)
            ->add('contrasenia', PasswordType::class)
            ->add('persona', EntityType::class, ['class' => Persona::class, 'choice_label' => 'nombres', 'label' => 'Nombre persona'])
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $form->getData();
            $entityManager->flush();
            
            return $this->redirectToRoute('ver_usuarios');
        }
    
        return $this->render('usuario/usuario.html.twig', ['form' => $form->createView(),]);   
    }

    /**
     * @Route("/eliminar_usuario/{id}", name="eliminar_usuario")
     */
    public function eliminar_usuario($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $usuario = $entityManager->getRepository(Usuario::class)->find($id);

        $entityManager->remove($usuario);
        $entityManager->flush();

        return $this->redirectToRoute('ver_usuarios');
    }

    /**
    * @Route("/iniciar_sesion", name="iniciar_sesion")
    */
    public function iniciar_sesion(Request $request)
    {

        $form = $this->createFormBuilder()
            ->add('usuario', TextType::class)
            ->add('contrasenia', PasswordType::class)
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario = $request->request->get('contrasenia');
            echo "contraseña"; print_r($usuario);
        //    exit;
        
            return $this->redirectToRoute('ver_usuarios');
        }

        return $this->render('security/login.html.twig', ['form' => $form->createView(),]);
    }
}
