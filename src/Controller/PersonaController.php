<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Persona;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class PersonaController extends AbstractController
{
    /**
     * @Route("/persona", name="persona")
     */
    public function registrar_persona(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $persona = new Persona();

        $form = $this->createFormBuilder($persona)
            ->add('identificacion', IntegerType::class)
            ->add('nombres', TextType::class)
            ->add('apellidos', TextType::class)
            ->add('correo', EmailType::class)
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();
    
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $persona = $form->getData();
            $entityManager->persist($persona);
            $entityManager->flush();
        
            return $this->redirectToRoute('ver_personas');
        }
        
        return $this->render('persona/persona.html.twig', ['form' => $form->createView(),]);
    }

    /**
     * @Route("/ver_personas", name="ver_personas")
     */
    public function ver_personas()
    {
        $personas = $this->getDoctrine()->getRepository(Persona::class)->findAll();
        return $this->render('persona/ver_personas.html.twig', ['personas' => $personas]);

    }

    /**
     * @Route("/actualizar_persona/{id}", name="actualizar_persona")
     */
    public function actualizar_persona(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $persona = $entityManager->getRepository(Persona::class)->find($id);
        
        if (!$persona) {
            throw $this->createNotFoundException('No Existen la persona con id'.$id);
        }
        $form = $this->createFormBuilder($persona)
            ->add('identificacion', IntegerType::class)
            ->add('nombres', TextType::class)
            ->add('apellidos', TextType::class)
            ->add('correo', EmailType::class)
            ->add('save', SubmitType::class, ['label' => 'Guardar'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $persona = $form->getData();
            $entityManager->flush();
            
            return $this->redirectToRoute('ver_personas');
        }
    
        return $this->render('persona/persona.html.twig', ['form' => $form->createView(),]);   
    }

    /**
     * @Route("/eliminar_persona/{id}", name="eliminar_persona")
     */
    public function eliminar_persona($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $persona = $entityManager->getRepository(Persona::class)->find($id);

        $entityManager->remove($persona);
        $entityManager->flush();

        return $this->redirectToRoute('ver_personas');
    }
}